package com.example.sheilambadi.bmicalculator.Model;

import com.orm.SugarRecord;

/**
 * Created by sheilambadi on 5/1/17.
 */

public class Bmi extends SugarRecord<Bmi>{
    String bmi;

    public Bmi() {
    }

    public Bmi(String bmi) {
        this.bmi = bmi;
    }

    @Override
    public String toString() {
        return bmi;
    }

}
