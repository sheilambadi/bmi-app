package com.example.sheilambadi.bmicalculator.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.sheilambadi.bmicalculator.Model.Bmi;
import com.example.sheilambadi.bmicalculator.R;

import java.util.List;

public class ListResults extends AppCompatActivity {
    ListView listView;
    ArrayAdapter<Bmi> adapter;
    Toolbar toolbarList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_results);

        listView = (ListView) findViewById(R.id.listView);

        List<Bmi> list = Bmi.listAll(Bmi.class);

        adapter = new ArrayAdapter<Bmi>(ListResults.this,android.R.layout.simple_list_item_1,list);
        listView.setAdapter(adapter);
    }
}
