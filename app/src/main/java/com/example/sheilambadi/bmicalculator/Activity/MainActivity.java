package com.example.sheilambadi.bmicalculator.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sheilambadi.bmicalculator.Model.Bmi;
import com.example.sheilambadi.bmicalculator.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    EditText height;
    EditText weight;
    TextView bmiResults;

    Bmi bmiObject = new Bmi();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //views
        height = (EditText) findViewById(R.id.heightText);
        weight = (EditText)findViewById(R.id.weightText);
        bmiResults = (TextView)findViewById(R.id.bmiResults);

        //intent to go to listview
        Button viewList = (Button)findViewById(R.id.viewResultsButton);
        viewList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //opening activity with listview
                Intent intent = new Intent(MainActivity.this, ListResults.class);
                startActivity(intent);
                //placing focus on height EditText
                height.setFocusableInTouchMode(true);
                height.requestFocus();
            }
        });
    }

    //method to hide soft keyboard out of edittext
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

    public void CalculateBmi(View view){
        //get values entered by the user
        String heightStr = height.getText().toString();
        String weightStr = weight.getText().toString();

        //check if user has not entered values
        if(heightStr != null && !"".equals(heightStr)
                && weightStr != null && !"".equals(weightStr)) {
            //cast values entered to float
            float heightValue = Float.parseFloat(heightStr) / 100;
            float weightValue = Float.parseFloat(weightStr);

            float bmi = weightValue / (heightValue * heightValue);
            bmiObject=new Bmi(""+bmi);
            bmiObject.save();
            bmiResults.setText("Bmi is " + bmi);

            Toast.makeText(this, "BMI successfully calculated", Toast.LENGTH_LONG).show();

            height.setText("");
            weight.setText("");
            height.setFocusableInTouchMode(true);
            height.requestFocus();
        } else {
            Toast.makeText(this, "Enter height and weight", Toast.LENGTH_LONG).show();
        }
    }


}
